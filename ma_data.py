from sqlalchemy import create_engine, ForeignKey, Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
import requests
import re
from bs4 import BeautifulSoup
from datetime import datetime, date
from dateutil import parser
from time import sleep

Base = declarative_base()
engine = create_engine("sqlite:///ma_data.db")
Session = sessionmaker(bind=engine)
session = Session()


class MAArtist(Base):
    __tablename__ = "ma_artist"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    country = Column(String)
    url = Column(String)

    def __repr__(self):
        return f"<MAArtist(name='{self.name}', country='{self.country}')>"


class MAAlbum(Base):
    __tablename__ = "ma_album"
    id = Column(Integer, primary_key=True)
    title = Column(String)
    artist_name = Column(String)
    date = Column(Date)
    artist_id = Column(Integer, ForeignKey("ma_artist.id"), nullable=True)
    artist = relationship("MAArtist")

    def __repr__(self):
        return f"<MAAlbum(title='{self.title}', artist='{self.artist_name}', year='{self.year}')>"


Base.metadata.create_all(engine)


def fetch_album(artist: str, title: str):

    existing = session.query(MAAlbum).filter_by(title=title, artist_name=artist).first()
    if existing:
        return existing
    sleep(0.5)

    r = requests.get(
        "https://www.metal-archives.com/search/ajax-advanced/searching/albums",
        params={"bandName": artist, "releaseTitle": title},
        headers={"User-Agent": "fleshgolem"},
    )
    try:
        json = r.json()
    except Exception as e:
        print(r.text)
        raise e
    if len(json["aaData"]) > 0:
        album_link = json["aaData"][0][1]
        artist_link = json["aaData"][0][0]
        album_url = extract_url(album_link)
        artist_url = extract_url(artist_link)
        date = get_date_from_album_page(album_url)
        ma_artist = get_artist_from_page(artist_url, artist)
        ma_album = MAAlbum()
        ma_album.artist = ma_artist
        ma_album.artist_name = artist
        ma_album.title = title
        ma_album.date = date
        session.add(ma_album)
        session.commit()
        return ma_album
    else:
        print(f"Album {artist} - {title} not found on MA")


def get_date_from_album_page(album_url) -> date:
    r = requests.get(url=album_url, headers={"User-Agent": "fleshgolem"})
    soup = BeautifulSoup(r.text, "html.parser")
    album_info = soup.find(id="album_info")
    left_tags = album_info.find(class_="float_left")
    date_str = left_tags.find_all("dd")[1].text
    date = parser.parse(date_str).date()
    return date


def get_artist_from_page(artist_url, name) -> MAArtist:

    existing = session.query(MAArtist).filter_by(url=artist_url).first()
    if existing:
        return existing

    r = requests.get(url=artist_url, headers={"User-Agent": "fleshgolem"})
    soup = BeautifulSoup(r.text, "html.parser")
    album_info = soup.find(id="band_info")
    left_tags = album_info.find(class_="float_left")
    country = left_tags.find_all("dd")[0].contents[0].text
    artist = MAArtist()
    artist.name = name
    artist.country = country
    artist.url = artist_url
    session.add(artist)
    session.commit()
    return artist


def extract_url(link: str):
    match = re.search(r"<a href=\"(.*?)\".*>.*</a>.*", link)
    if match:
        return match.group(1)


def enter_manually(artist: str, title: str, datestr: str, country: str) -> MAAlbum:
    ma_band = MAArtist()
    ma_band.country = country
    ma_band.name = artist
    ma_band.url = ""
    ma_album = MAAlbum()
    ma_album.artist = ma_band
    ma_album.title = title
    ma_album.date = parser.parse(datestr).date() if datestr else None
    ma_album.artist_name = artist
    session.add(ma_album)
    session.commit()
    return ma_album
