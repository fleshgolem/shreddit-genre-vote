import http.server
import socketserver
import threading
import webbrowser
import sys
import time

PORT = 8000
DIRECTORY = "_output"


class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)


def start_server():
    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print("serving at port", PORT)
        httpd.serve_forever()


threading.Thread(target=start_server).start()
url = "http://127.0.0.1:8000"
webbrowser.open_new(url)

while True:
    try:
        time.sleep(1)
    except KeyboardInterrupt:
        sys.exit(0)
