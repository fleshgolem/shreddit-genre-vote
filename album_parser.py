from typing import List, Mapping
from collections import defaultdict
import yaml
import csv
from model import Album, Vote
import sys
from image_renderer import render_image
from openpyxl import Workbook
from ma_data import fetch_album, enter_manually
import prompt
import re
import requests


def parse_csv_input(filename) -> List[str]:
    reader = csv.reader(open(filename, encoding="utf8"))
    output = []
    for row in reader:
        output.append(row[1])
    return output


def parse_reddit_input(filename):
    data = requests.get(
        f"{filename}.json?limit={1000}", headers={"User-agent": "Shreddit parser"}
    ).json()
    comments = data[1]["data"]["children"]
    ignored_posts = yaml.load(open("ignored_posts.yml"), Loader=yaml.CLoader)
    bodies = [
        c["data"]["body"] for c in comments if c["data"]["id"] not in ignored_posts
    ]
    late_additions = yaml.load(open("late_additions.yml"), Loader=yaml.CLoader)
    return bodies + late_additions


def parse_votes(filename, autocorrections=None, corrections=None) -> List[Vote]:
    votes: List[Vote] = []

    reddit_pattern = r"reddit\.com\/"
    if re.search(reddit_pattern, filename):
        bodies = parse_reddit_input(filename)
        for b in bodies:
            votes.append(
                Vote(b, autocorrections=autocorrections, corrections=corrections)
            )
    else:
        for text in parse_csv_input(filename):
            votes.append(
                Vote(text, autocorrections=autocorrections, corrections=corrections)
            )

    return votes


if __name__ == "__main__":
    auto = yaml.load(open("autocorrect.yml", encoding="utf8"), Loader=yaml.FullLoader)
    correct = yaml.load(
        open("corrections.yml", encoding="utf8"), Loader=yaml.FullLoader
    )

    votes = parse_votes(sys.argv[1], autocorrections=auto, corrections=correct)

    album_results: Mapping[Album, int] = defaultdict(int)
    for v in votes:
        for a in v.albums:
            album_results[a] += 1

    top_albums = sorted(album_results.items(), key=lambda item: item[1], reverse=True)

    scrape = prompt.character(
        "Do you want to scrape MA for year/country info (This might take some time)?\n (y/N)\n"
    )
    if scrape == "y":
        for a, _ in top_albums:
            print(f"Fetching {a}")
            ma_album = fetch_album(a.artist, a.title)
            if not ma_album:
                print("Not found on MA, please enter manually")
                date = prompt.string(
                    "Date of Release (format: YYYY-MM-DD)\n", empty=True
                )
                country = prompt.string("Country of Origin\n", empty=True)
                ma_album = enter_manually(
                    artist=a.artist, title=a.title, datestr=date, country=country
                )
            a.country = ma_album.artist.country
            a.date = ma_album.date

    wb = Workbook()
    ws1 = wb.create_sheet("Top Albums")

    with open("top_albums.txt", "w", encoding="utf8") as f:
        for idx, (album, count) in enumerate(top_albums):
            f.write(f"{idx + 1}\t {album.artist} - {album.title} \t {count}\n")
            ws1[f"A{idx + 1}"] = album.artist
            ws1[f"B{idx + 1}"] = album.title
            ws1[f"C{idx + 1}"] = count

            if album.date:
                ws1[f"D{idx + 1}"] = album.date.year

            if album.country:
                ws1[f"E{idx + 1}"] = album.country

    band_results: Mapping[str, int] = defaultdict(int)
    for v in votes:
        for a in v.albums:
            if "/" in a.artist:
                s = a.artist.split("/")
                a1 = s[0].strip()
                band_results[a1] += 1
                a2 = s[1].strip()
                band_results[a2] += 2
            else:
                band_results[a.artist] += 1

    ws2 = wb.create_sheet("Top Bands")

    with open("top_bands.txt", "w", encoding="utf8") as f:
        for idx, (band, count) in enumerate(
            sorted(band_results.items(), key=lambda item: item[1], reverse=True)
        ):
            f.write(f"{idx + 1}\t {band} \t {count}\n")
            ws2[f"A{idx + 1}"] = band
            ws2[f"B{idx + 1}"] = count

    ws3 = wb.create_sheet("All Albums")
    with open("all_albums.txt", "w", encoding="utf8") as f:
        for idx, album in enumerate(
            sorted(album_results.keys(), key=lambda item: str(item))
        ):
            f.write(f"{album.artist} - {album.title}\n")
            ws3[f"A{idx + 1}"] = album.artist
            ws3[f"B{idx + 1}"] = album.title

    if scrape == "y":
        ws4 = wb.create_sheet("Country Distribution")
        country_counts = defaultdict(int)
        for album, count in top_albums:
            if album.country:
                country_counts[album.country] += count
        for idx, (country, count) in enumerate(
            sorted(country_counts.items(), key=lambda item: item[1], reverse=True)
        ):
            ws4[f"A{idx + 1}"] = country
            ws4[f"B{idx + 1}"] = count

    top_25 = [a for (a, _) in top_albums[:25]]
    render_image(top_25)
    del wb["Sheet"]
    wb.save("output.xlsx")
