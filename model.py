from dataclasses import dataclass, field
from typing import List, Mapping, Optional
from titlecase import titlecase
from datetime import date, datetime
import re
import string
import html


@dataclass
class VoteException(Exception):
    message: str


@dataclass
class Album:
    artist: str
    title: str
    datestr: Optional[str] = field(default=None, compare=False, hash=False)
    country: Optional[str] = field(default=None, compare=False, hash=False)

    @property
    def date(self) -> Optional[date]:
        if self.datestr:
            return datetime.strptime(self.datestr, "%Y-%m-%d").date()
        return None

    @date.setter
    def date(self, value: date):
        if value:
            self.datestr = value.strftime("%Y-%m-%d")
        else:
            self.datestr = None

    def __init__(self, str: str, autocorrections=None, corrections=None):
        # i have no idea where these backslashes come from, but apparently that can happen with reddit formatting and should probably never appear in an actual title
        str = str.replace("\\", "")
        if str.strip() == "":
            raise VoteException("")
        if autocorrections is None:
            autocorrections = {}
        if corrections is None:
            corrections = {}
        if not "_" in str and len(str.split("-")) == 2:
            # Googa used dash instead of underscore
            parts = str.split("-")
            str = f"{parts[0].strip()}_{parts[1].strip()}"

        # Apply corrections for the first time for thinngs that are just completely wrong
        if str in autocorrections:
            str = autocorrections[str]
        if str in corrections:
            str = corrections[str]

        if "_" in str:
            try:
                a, t = str.split("_")
            except ValueError:
                raise VoteException(f"Invalid format '{str}'")
            a = titlecase(a.strip())
            t = titlecase(t.strip())
            str = f"{a}_{t}"
            # Apply Corrections for a second time so we also catch whitespace and capitalization errors now
            if str in autocorrections:
                str = autocorrections[str]
            if str in corrections:
                str = corrections[str]
            if not str:
                raise VoteException("Empty entry")
            if str == "Darkspace_Dark Space II:a":
                raise VoteException(f"Invalid format '{str}'")
            a, t = str.split("_")
            self.artist = titlecase(a.strip())
            self.title = titlecase(t.strip())
        else:
            raise VoteException(f"Invalid format '{str}'")

    def internal_title(self):
        return f"{self.artist}_{self.title}"

    @property
    def filename(self) -> str:
        valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)

        return "".join(c for c in str(self) if c in valid_chars)

    def __repr__(self):
        return self.internal_title()

    def __hash__(self):
        return str(self).__hash__()

    def __eq__(self, other):
        return self.artist == other.artist and self.title == other.title


@dataclass
class Vote:
    albums: List[Album]

    enumeration_re = re.compile(r"^\d*\.\s+")

    def __init__(self, str: str, autocorrections=None, corrections=None):
        self.albums = []
        entries = str.split("\n")
        entries = self._correct_comma_separation(entries)
        entries = self._remove_enumeration(entries)
        for s in entries:
            try:
                self.albums.append(
                    Album(s, autocorrections=autocorrections, corrections=corrections)
                )
            except VoteException as e:
                if e.message != "":
                    print(e)
        self.albums = list(set(self.albums))
        if len(self.albums) > 25:
            self.albums = self.albums[:25]

    def _correct_comma_separation(self, entries: List[str]) -> List[str]:
        not_empty = [s.strip() for s in entries if s.strip() != ""]
        if len(not_empty) == 1:
            return not_empty[0].split(",")
        return not_empty

    def _is_enumerated(self, str: str) -> bool:
        return self.enumeration_re.match(str) is not None

    def _remove_enumeration(self, entries: List[str]) -> List[str]:

        result = []
        for l in entries:
            l = html.unescape(l)
            # Trim for good measure
            l = l.strip()
            # Remove leading control characters
            if l.startswith("-"):
                l = l[1:]
            if l.startswith("*"):
                l = l[1:]
            if l.startswith("•"):
                l = l[1:]
            if l.endswith(","):
                l = l[:-1]
            # I guess i need regex for ordered lists
            l = re.sub("^\\d*\\.\\s*", "", l)
            # Trim again, because we prolly left whitespace in there along the way
            l = l.strip()
            result.append(l)
        return result


@dataclass
class Typo:
    options: List[Album]


@dataclass
class Correction:
    correct: Album
    wrong: List[Album]
