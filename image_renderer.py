from model import Album
from typing import List, Iterable
from os import path
import os
import requests
import itertools
from jinja2 import Environment, FileSystemLoader, select_autoescape
import subprocess
import string
import shutil


def chunks(iterable: Iterable, size: int) -> Iterable:
    it = iter(iterable)
    chunk = tuple(itertools.islice(it, size))
    while chunk:
        yield chunk
        chunk = tuple(itertools.islice(it, size))


def render_image(albums: List[Album]):
    try:
        from _secrets import LAST_FM_KEY
    except ImportError:
        return
    if not path.exists("_output"):
        os.mkdir("_output")
    for album in albums:
        retrieve_album_art(album, LAST_FM_KEY)
    rows = chunks(albums, 5)
    env = Environment(
        loader=FileSystemLoader("templates"),
        autoescape=select_autoescape(["html", "xml"]),
    )
    template = env.get_template("grid.html")
    open("_output/index.html", "w", encoding="utf8").write(
        template.render(rows=rows, albums=albums)
    )


def retrieve_album_art(album: Album, key: str):
    filename = path.join(f"_output/{album.filename}.png")
    if path.exists(filename):
        return
    query = f"{album.artist} - {album.title}"
    album_req = requests.get(
        f"https://ws.audioscrobbler.com/2.0/?method=album.search&album={query}&api_key={key}&format=json",
        headers={"User-Agent": "Top Album Chart Maker THingy"},
    )
    album_json = album_req.json()
    matches = album_json["results"]["albummatches"]["album"]
    if len(matches) == 0:
        print(f"Album not found: {album}")
        return
    images = matches[0]["image"]
    image = [i for i in images if i["size"] == "large"][0]

    url = image["#text"]

    if url:
        image_req = requests.get(url)
        image_req.raw.decode_content = True
        with open(filename, "wb") as f:
            for chunk in image_req:
                f.write(chunk)
    else:
        shutil.copy("cancel.png", filename)
