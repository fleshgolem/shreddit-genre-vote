Shreddit Genre Vote Counter
==========================

Installation
------------

* Install python 3.7 or higher (This makes heavy use of python 3.7 features, 3.6 will not work)
* Ideally run this in a virtualenv, but not required

```
python3.7 -m venv venv
source venv/bin/activate
```

* Install required packages

```
pip install -r requirements.txt
```
  
Usage
-----
 
* Add CSV output file from google forms to folder (e.g. "Trad_Final_Raw.txt")
* Scan votes for potential typos: `python corrections.py Trad_Final_Raw.txt`
    * Choose correct options for all potential errors
* Run preliminary tally `python album_parser.py Trad_Final_Raw.txt`
    * Look at terminal output for all entries with invalid formats. Add corrections for these to "corrections.yml" or ignore them if appropriate
* Look at the output file "all_albums.txt" to spot additional errors. Add corrections for those to "corrections.yml"
* Run `python album_parser.py Trad_Final_Raw.txt` again. Tally is in files "top_albums.txt" and "top_bands.txt" as well as "out.xlsx"

Image Rendering
-----

You can optionally render a neverendingchartrendering style image of the results, for this you will need to have a last.fm api key, which you can get at https://www.last.fm/api/account/create

* Create a file `_secrets.py`
* Enter the line `LAST_FM_KEY = "you api key"` (including quotes)
* Run `python album_parser.py Trad_Final_Raw.txt` as per usual. It will take way more time to run this time, because it needs to download all the covers
* There should be a folder named "_output" with a bunch of images and an "index.html" file now
* Run `python serve_image.py`. It should open your browser and immediately download the chart image for you

Metal Archives Integration
-----

* Scraping MA adds country of origin and release year info to the output (only the xlsx file)
* Since it can take quite a while, the parser will ask you, if you want to do it
* All results are cached locally, so while the first run will take some time, any subsequent ones should go much faster
* If any album cannot be found on MA, the parser will ask you to enter the info manually. You can either
    * Exit the script (Ctrl+C) and fix the spelling if it is not in accordance with MA
    * Enter the info (it will get stored for later runs)
    * Enter nothing (which will get stored as well)

Known Issues
-----------

* If the typo scanner finds more than 2 releases, some of which are the same with typos but some are also completely different (e.g. "Black Sabbath Vol. 4", "Black Sabbath Vol 4" and "Black Sabbath"), it is impossible to only partly apply autocorrections.

    I have no idea how to fix this without making the prompts overly complex, so right now, these just have to be corrected manually.
